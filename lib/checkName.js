'use strict';

var Promise = require('pinkie-promise');

module.exports = function checkName(client, name) {
  return client.register()
    .then(function registered() {
      return client.checkName(name);
    })
    .then(function complete(results) {
      client.dispose();
      return results;
    })
    .catch(function error(err) {
      client.dispose();
      return Promise.reject(err);
    });
};
