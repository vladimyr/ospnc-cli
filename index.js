'use strict';

var createUUID = require('uuid').v4;
var format = require('util').format;
var debug = require('debug')('client');
var io = require('socket.io-client');
var Promise = require('pinkie-promise');

var noop = Function.prototype;

function Client(options) {
  options = options || {};
  var domain = options.domain || 'ivantomic.com';
  this._server = format('ws://%s:8089', domain);
}
Client.forge = function() {
  return new Client();
};

Client.prototype.register = function(callback) {
  callback = callback || noop;

  var socket = io.connect(this._server);
  this.socket = socket;

  debug('Creating new uuid...');
  var uuid = createUUID();
  this.uuid = uuid;
  debug('uuid=%s', uuid);

  socket.on('connect', function() {
    debug('Invoking "#register" with client uuid=%s', uuid);
    socket.emit('register', { guid: uuid });
  });

  return new Promise(function(resolve, reject) {
    socket.on('connect_error', function(err) {
      debug('Connection failed: %s', err.message);
      callback(err);
      reject(err);
    });

    socket.on('welcome', function(data) {
      var uuid = data.guid;
      debug('Client %s is registered', uuid);
      callback(null);
      resolve();
    });
  });
};

Client.prototype.checkName = function(name, callback) {
  callback = callback || noop;

  if (!name) {
    var err = new Error('Name is not provided!');
    return Promise.reject(err);
  }

  var results = [];

  debug('Invoking "#checkName" with client uuid=%s and search=%s',
    this.uuid, name);

  this.socket.emit('process', {
    guid: this.uuid,
    clientSearch: name
  });

  var socket = this.socket;
  return new Promise(function(resolve, reject) {
    socket.on('console_output', function(data) {
      debug('Received query response: %j', data.data);

      // blacklisting SourceForge
      if (data.data.name === 'SourceForge')
        return;

      var len = results.push(data.data);
      debug('%d results collected', len);

      if (len === 8) {
        debug('All results collected');
        results.sort(function(a,b) {
          return a.name.localeCompare(b.name);
        });
        callback(null, results);
        resolve(results);
      }
    });
  });
};

Client.prototype.dispose = function() {
  debug('Closing internal socket...');
  this.socket.close();
  this.uuid = null;
  debug('Socket closed, uuid removed');
};

module.exports = Client;
